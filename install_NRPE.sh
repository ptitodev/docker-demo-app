#! /bin/sh

user="root"
ABSPATH=$(readlink -f "$0") # /usr/local/nagios/etc/NRPE/install_NRPE.sh
SCRIPTPATH=$(dirname "$ABSPATH") # /usr/local/nagios/etc/NRPE
CFG_Files="/usr/local/nagios/etc/objects/servers/*"
#Exclude_CFG="localhost.cfg|figaro.cfg|Ubuntu.cfg"

function Which_Server () {
	read -p "Input you will install server(ip address):" ip_address
#	read -p "Input which user installing:" user
	
	stty_orig=`stty -g`
	stty -echo
	read -p "Input $user password: " password
	stty $stty_orig

#	echo "You will install $ip_address, using $user, the password is $password"
}

function Auto_Execute () {

	expect -c "
	set timeout 300
	spawn bash -c \"$1\"
	expect \"*password*\"
	send \"$password\r\"
	expect ]#
	"
}

function Update_All_Servers () {

        stty_orig=`stty -g`
        stty -echo
        read -p "Input $user password: " password
        stty $stty_orig

	echo -e "\n=====Update Servers====="
	grep address `grep check_memory $CFG_Files|grep -v "#"|awk '{print $1}'|sed 's/.$//'`|grep -v ambtwubu|grep -v ambtw-gpu|grep -v ambtw-vnc|grep -v Ubuntu| awk '{print $3}'
	echo "========================"
	read -p "Do you update servers(y/n):" yn

	if [ "$yn" == "Y" ] || [ "$yn" == "y" ]
	then
		for each_ip_address in $(grep address `grep check_memory $CFG_Files|grep -v "#"|awk '{print $1}'|sed 's/.$//'`|grep -v ambtwubu|grep -v ambtw-gpu|grep -v ambtw-vnc|grep -v Ubuntu| awk '{print $3}')
		do
			Auto_Execute "ssh $user@$each_ip_address mkdir /tmp/NRPE/"
			Auto_Execute "scp -r $SCRIPTPATH/* $user@$each_ip_address:/tmp/NRPE/"

			# For Red Hat
			Auto_Execute "ssh $user@$each_ip_address /tmp/NRPE/script/update_plugins_redhat.sh"
			# For Ubuntu
			#Auto_Execute "ssh $user@$each_ip_address /tmp/NRPE/script/update_plugins_ubuntu.sh"

			Auto_Execute "ssh $user@$each_ip_address rm -rf /tmp/NRPE/"
			#echo "ssh $user@$each_ip_address /tmp/NRPE/script/update_plugins_ubuntu"
		done
	elif [ "$yn" == "N" ] || [ "$yn" == "n" ]
	then
		exit 0
	else
		echo "You don't chice which one."
	fi
}

echo "1. Install nrpe(Red Hat)"
echo "2. Install nrpe(Ubuntu)"
echo "3. Update plugins & nrpe(Red Hat)"
echo "4. Update plugins & nrpe(Ubuntu)"
echo "5. Install nrpe(Red Hat 7.8)"
echo "6. Install nrpe(Red Hat 8.2)"
#echo ". Uninstall nrpe(Red Hat)"
#echo ". Reinstall nrpe(Red Hat)"
#echo "6. Update plugins & nrpe(All)"
read -p "Does your choice(1~5):" choice

case $choice in 
	"1")
		Which_Server
		Auto_Execute "ssh $user@$ip_address mkdir /tmp/NRPE/"
		Auto_Execute "scp -r $SCRIPTPATH/* $user@$ip_address:/tmp/NRPE/"
		Auto_Execute "ssh $user@$ip_address /tmp/NRPE/script/install_redhat.sh"
		Auto_Execute "ssh $user@$ip_address rm -rf /tmp/NRPE/"
		;;
	"2")
		Which_Server
		Auto_Execute "ssh $user@$ip_address mkdir /tmp/NRPE/"
		Auto_Execute "scp -r $SCRIPTPATH/* $user@$ip_address:/tmp/NRPE/"
		Auto_Execute "ssh $user@$ip_address /tmp/NRPE/script/install_ubuntu.sh"
		Auto_Execute "ssh $user@$ip_address rm -rf /tmp/NRPE/"
		;;
	"3")
		Which_Server
		Auto_Execute "ssh $user@$ip_address mkdir /tmp/NRPE/"
		Auto_Execute "scp -r $SCRIPTPATH/* $user@$ip_address:/tmp/NRPE/"
		Auto_Execute "ssh $user@$ip_address /tmp/NRPE/script/update_plugins_redhat.sh"
		Auto_Execute "ssh $user@$ip_address rm -rf /tmp/NRPE/"
		;;
	"4")
		Which_Server
		Auto_Execute "ssh $user@$ip_address mkdir /tmp/NRPE/"
		Auto_Execute "scp -r $SCRIPTPATH/* $user@$ip_address:/tmp/NRPE/"
		Auto_Execute "ssh $user@$ip_address /tmp/NRPE/script/update_plugins_ubuntu.sh"
		Auto_Execute "ssh $user@$ip_address rm -rf /tmp/NRPE/"
		;;
        "5")
                Which_Server
                Auto_Execute "ssh $user@$ip_address mkdir /tmp/NRPE/"
                Auto_Execute "scp -r $SCRIPTPATH/* $user@$ip_address:/tmp/NRPE/"
                Auto_Execute "ssh $user@$ip_address /tmp/NRPE/script/install_redhat_7.8.sh"
                Auto_Execute "ssh $user@$ip_address rm -rf /tmp/NRPE/"
                ;;
        "6")
                Which_Server
                Auto_Execute "ssh $user@$ip_address mkdir /tmp/NRPE/"
                Auto_Execute "scp -r $SCRIPTPATH/* $user@$ip_address:/tmp/NRPE/"
                Auto_Execute "ssh $user@$ip_address /tmp/NRPE/script/install_redhat_8.2.sh"
                Auto_Execute "ssh $user@$ip_address rm -rf /tmp/NRPE/"
                ;;
#	"5")
#		Which_Server
#		Auto_Execute "scp -r NRPE/ $user@$ip_address:/tmp/"
#		Auto_Execute "ssh $user@$ip_address /tmp/NRPE/update_plugins.sh"
#		Auto_Execute "ssh $user@$ip_address rm -rf /tmp/NRPE/"
#		echo "Not Ready"
#		;;
#	"6")
#		Update_All_Servers
#		;;
	"ipmi")
		Which_Server
		Auto_Execute "ssh $user@$ip_address mkdir /tmp/NRPE/"
		Auto_Execute "scp -r /usr/local/nagios/etc/NRPE/* $user@$ip_address:/tmp/NRPE/"
		Auto_Execute "ssh $user@$ip_address /tmp/NRPE/script/setup_ipmi_plugin_redhat.sh"
		Auto_Execute "ssh $user@$ip_address rm -rf /tmp/NRPE/"
		;;
	*)
		echo "Please input 1~5"
		;;
esac

